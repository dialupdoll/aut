#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
pub trait ImplementationProvider {
    fn new_window() -> Box<dyn interface::Window>;
}

pub mod interface {

    pub type TabOrderID = u32;

    use crate::interface::events::Event;
    use crate::interface::rendering::RenderSurface;
    use std::sync::mpsc::Receiver;

    pub trait Window {
        /*fn new(dyn Size);*/
        // we shall leave this open ended, for now
        fn show(&mut self);
        fn hide(&self);
        fn add(&self, component: Box<dyn Component>);
        fn size_get(&self) -> Box<dyn coordinates::Size>;
        fn surface(&self) -> Box<dyn rendering::RenderSurface>;

        // functions below are for event flow management
        fn poll_events(&mut self) -> Event;

        fn drop_all_events(&self);
    }
    pub mod coordinates {
        /*  *
         *  For our coordinate system, we shall work in percentages. this allows for any underlying
         *  coordinate system, and in all my mathematical testing, plenty of precision.
         */
        pub trait Location {
            fn x_location_screen(&self) -> f32;
            fn y_location_screen(&self) -> f32;

            fn x_location_window(&self) -> f32;
            fn y_location_window(&self) -> f32;
        }
        pub trait Size {
            fn horizontal_width_screen(&self) -> f32;
            fn vertical_height_screen(&self) -> f32;

            fn horizontal_width_window(&self) -> f32;
            fn vertical_height_window(&self) -> f32;
        }
        pub trait Rect2D: Size + Location {
            fn overlap_check(first: Self, second: Self) -> bool;
        }
    }
    pub mod rendering {
        pub trait RenderSurface {}
    }

    pub trait Component {
        fn tabOrderID(&self) -> u32;
    }
    pub trait Layout {}
    pub mod events {
        pub enum ClickType {
            ButtonDown,
            ButtonRelease,
        }

        pub trait EventProperties {
            fn subjectID(&self) -> u32;
            fn string_description(&self) -> u32;
        }

        pub enum Event {
            Clicked(ClickType),
            WindowClosed,
            ALL,
            UNSUPPORTED,
            NONE,
        }
        pub trait EventProvider {}
        // TODO: CHANNELS AND MESSAGE PASSING
        // https://doc.rust-lang.org/book/ch16-02-message-passing.html
        // https://doc.rust-lang.org/std/sync/mpsc/index.html
    }
}
