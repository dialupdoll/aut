use abstract_ui_toolkit::interface::events::Event;
use abstract_ui_toolkit::ImplementationProvider;
use std::thread;
use std::time::Duration;

fn main() {
    let mut window = sdl_aut_implementation::SdlAutProvider::new_window();
    window.show();
    loop {
        match window.poll_events() {
            Event::WindowClosed => return,
            NONE => {
                thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
            }
        }
    }
}
