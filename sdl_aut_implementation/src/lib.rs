#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

mod internal_use {
    // THIS IS WRONG - I'm intimidated by the sdl2 methods
    // for getting the screen size - they're unsafe so idk what to do
    const MAX: i32 = i32::MAX / 4;
    fn width_of_screen() -> i32 {
        MAX
    }
    fn height_of_screen() -> i32 {
        MAX
    }
}
struct SizeSdl2Aut {
    width: i32,
    height: i32,
}
struct LocationSDL2_AUT {
    x: i32,
    y: i32,
}

use abstract_ui_toolkit::interface::{Component, Window};
use std::thread;

struct WindowSdl2Aut {
    sdl_underlying: Option<sdl2::video::Window>,
    sdl_context: Box<Sdl>,
}
impl Window for WindowSdl2Aut {
    fn show(&mut self) {
        match &self.sdl_underlying {
            Some(_) => println!("Window already shown!"), // might need more checks to ensure
            // window actually is open.
            None => {
                let video_subsystem = self.sdl_context.as_ref().video().unwrap();

                let window = video_subsystem
                    .window("rust-sdl2 demo", 800, 600)
                    .position_centered()
                    .build()
                    .unwrap();

                let mut canvas = window.into_canvas().build().unwrap();
                canvas.set_draw_color(Color::RGB(0, 255, 255));
                canvas.clear();
                canvas.present();
                self.sdl_underlying = Some(canvas.into_window());
            }
        }
    }

    fn hide(&self) {
        todo!()
    }

    fn add(&self, component: Box<dyn Component>) {
        todo!()
    }

    fn size_get(&self) -> Box<dyn Size> {
        todo!()
    }

    fn surface(&self) -> Box<dyn RenderSurface> {
        todo!()
    }

    fn poll_events(&mut self) -> abstract_ui_toolkit::interface::events::Event {
        let event_pump = self.sdl_context.as_ref().event_pump();
        //dbg!();

        let mut canvas = self
            .sdl_underlying
            .take()
            .unwrap()
            .into_canvas()
            .build()
            .unwrap();

        let mut i = 0;
        i = (i + 1) % 255;
        canvas.set_draw_color(Color::RGB(i, 64, 255 - i));
        canvas.clear();
        self.sdl_underlying = Some(canvas.into_window());

        let event = event_pump.unwrap().poll_event();
        return match event {
            Some(
                Event::Quit { .. }
                | Event::Window {
                    win_event: WindowEvent::Close,
                    ..
                }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                },
            ) => abstract_ui_toolkit::interface::events::Event::WindowClosed,
            None => abstract_ui_toolkit::interface::events::Event::NONE,
            _ => abstract_ui_toolkit::interface::events::Event::UNSUPPORTED,
        };
        // The rest of the game loop goes here...
    }

    fn drop_all_events(&self) {
        todo!()
    }
}

pub struct SdlAutProvider {}
impl ImplementationProvider for SdlAutProvider {
    fn new_window() -> Box<dyn Window> {
        let mut result: WindowSdl2Aut;
        result = WindowSdl2Aut {
            sdl_underlying: Option::None,
            sdl_context: Box::new(sdl2::init().unwrap()),
        };
        return Box::new(result);
    }
}

//using f32 as decimal percentage of available area as coordinates

extern crate sdl2;

use abstract_ui_toolkit::interface::coordinates::Size;
use abstract_ui_toolkit::interface::events::ClickType::ButtonDown;
use abstract_ui_toolkit::interface::events::Event::NONE;
use abstract_ui_toolkit::interface::events::EventProperties;
use abstract_ui_toolkit::interface::rendering::RenderSurface;
use abstract_ui_toolkit::ImplementationProvider;
use sdl2::event::{Event, EventType, WindowEvent};
use sdl2::keyboard::Keycode;
use sdl2::libc::newlocale;
use sdl2::pixels::Color;
use sdl2::Sdl;
use std::time::Duration;

pub fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("rust-sdl2 demo", 800, 600)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();

    canvas.set_draw_color(Color::RGB(0, 255, 255));
    canvas.clear();
    canvas.present();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut i = 0;
    'running: loop {
        i = (i + 1) % 255;
        canvas.set_draw_color(Color::RGB(i, 64, 255 - i));
        canvas.clear();
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                _ => {}
            }
        }
        // The rest of the game loop goes here...

        canvas.present();
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }
}
